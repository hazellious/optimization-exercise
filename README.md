# OptimizationExercise

The goal of this repository is for aspiring Equalture developers to identify
improvements in the code in this repository.

Fork the project and create a Merge Request. Pretty much anything is allowed in
this MR, including adding libraries where you see fit. Improve style,
performance, readability and introduce best practices as much as possible.

After the MR has been reviewed, you are requested to remove the fork and the
merge request will be deleted, to keep things fair to future and past candidates.

Hint: There are some pointers in the tests and comments.
